import random

def ordenamiento_mezcla(lista):
    #Recursividad

    if len(lista) > 1:
        medio = len(lista)//2
        izquierda = lista[:medio] #Rebanar por la mitad izquierda la lista
        derecho = lista[medio:] #Rebanar por la mitad derecha la lista

        print(izquierda, '*' * 5, derecho)
        #Llamar recursivamente en cada mitad

        ordenamiento_mezcla(izquierda)
        ordenamiento_mezcla(derecho)

        # Iteradores para recorrer listas
        i = 0
        j = 0

        #I Iterador para lista principal K

        k = 0

        while i < len(izquierda) and j < len(derecho):
            if izquierda[i] < derecho[j]:
                lista[k] = izquierda[i]
                i +=1
            else:
                lista[k] = derecho[j]
                j +=1

            k +=1

        while i < len(izquierda):
            lista[k] = izquierda[i]
            i +=1
            k +=1
        while j < len(derecho):
            lista[k] = derecho[j]
            j +=1
            k +=1
        print(f'izquierda: {izquierda}, derecha: {derecho}')
        print(lista)
        print('#'*50)
    return  lista

if __name__ == '__main__':
    tamano_lista = int(input('Tamaño para la lista? '))

    lista = [random.randint(0, 100) for i in range(tamano_lista)]
    print(lista)
    print('*-*' * 20)

    lista_ordenada = ordenamiento_mezcla(lista)
    print(lista_ordenada)