from bokeh.plotting import figure, output_file, show

if __name__ == '__main__':
    output_file('grafica_simple.html')
    fig = figure()

    total_val = int(input('Cuantos valores graficar? '))
    x_val = list(range(total_val))
    y_vals = []

    for i in x_val:
        val = int(input(f'Valor para {i}: '))
        y_vals.append(val)

    fig.line(x_val, y_vals, line_width = 2)
    show(fig)